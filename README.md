# An Ansible Playbook to Update my Cloud Servers

Does the following things:

* Checks if all servers are online and have enough disk space. Aborts the run if not all servers are online or if a server has less than 3 GB.

* Runs an apt update/upgrade + delete old kernels + reboots a number of canary servers

* Waits for user input before proceeding with other servers

* Staggered updates as some VMs are on the same physical servers so only a few updates should be done at a time to speed up operation.
